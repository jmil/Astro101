# Astronomy 101 (Astro101)

This repo is is the files and lecture notes for [Astro101](https://jmilo.ir/astro101/), astronomy course. The course content is currently in Persian, but soon will translate into English.


# On the course
The course consists of ten theoretical and one observational sessions for intrested adults. There is no math (excluding the famous formula $E = mc^2$).

## Outline

1. Introduction
2. Earth, The Moon, and Calendar
3. Solar System
4. Sun and Stars
5. Astronomy, Space, and Technology
6. Cosmology and Astrobiology

and an observational session.
